#![allow(dead_code)]
use itertools::Itertools;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::fs;
use std::ops::Add;

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

type MapT = Vec<Vec<u32>>;

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
struct Point {
  x: i32,
  y: i32,
}

impl Add for Point {
  type Output = Self;

  fn add(self, other: Self) -> Self::Output {
    Self {
      x: self.x + other.x,
      y: self.y + other.y,
    }
  }
}

fn map_iter(map: &Map) -> MapIterator {
  MapIterator {
    w: map.w,
    h: map.h,
    index: 0,
  }
}

struct MapIterator {
  w: usize,
  h: usize,
  index: usize,
}

#[derive(Debug)]
struct Map {
  map: MapT,
  w: usize,
  h: usize,
}

impl Map {
  fn new(map: MapT) -> Map {
    Map {
      w: map[0].len(),
      h: map.len(),
      map,
    }
  }
  fn parse(s: &str) -> Map {
    Map::new(
      s.lines()
        .map(|line| {
          line
            .chars()
            .map(|c| c.to_digit(10).expect("Can't parse number"))
            .collect()
        })
        .collect(),
    )
  }
  fn at(&self, p: Point) -> &u32 {
    &self.map[p.y as usize][p.x as usize]
  }
}

impl Iterator for MapIterator {
  type Item = Point;

  fn next(&mut self) -> Option<Point> {
    if self.index >= self.w * self.h {
      return None;
    }
    let p = Point {
      x: (self.index % self.w) as i32,
      y: (self.index / self.w) as i32,
    };
    self.index += 1;
    Some(p)
  }
}

fn get_neighbors(p: Point, w: usize, h: usize) -> Vec<Point> {
  [
    Point { x: 1, y: 0 },
    Point { x: -1, y: 0 },
    Point { x: 0, y: 1 },
    Point { x: 0, y: -1 },
  ]
  .iter()
  .map(|&p2| p + p2)
  .filter(|&p| 0 <= p.x && p.x < w as i32 && 0 <= p.y && p.y < h as i32)
  .collect()
}

fn get_low_points(map: &Map) -> Vec<Point> {
  map_iter(&map)
    .filter(|&p| {
      get_neighbors(p, map.w, map.h)
        .iter()
        .all(|&p2| map.at(p) < map.at(p2))
    })
    .collect()
}

fn solution1() {
  let input = load();
  let map = Map::parse(&input);
  let answer: u32 = get_low_points(&map).iter().map(|&p| *map.at(p) + 1).sum();
  println!("answer: {}", answer);
}

// Performs a BFS at the given low-point, counting the size
fn get_basin_size(lp: &Point, map: &Map) -> usize {
  let mut size = 0;
  let mut q = VecDeque::from([*lp]);
  let mut visited = HashSet::new();
  visited.insert(*lp);
  while !q.is_empty() {
    let p = q.pop_front().unwrap();
    size += 1;
    let neighbors = get_neighbors(p, map.w, map.h);
    for &neighbor in neighbors.iter().filter(|n| {
      let v1 = *map.at(p);
      let v2 = *map.at(**n);
      v1 < v2 && v2 != 9
    }) {
      if !visited.contains(&neighbor) {
        visited.insert(neighbor);
        q.push_back(neighbor);
      }
    }
  }
  size
}

fn solution2() {
  let input = load();
  let map = Map::parse(&input);
  let lp = get_low_points(&map);
  println!(
    "answer: {:?}",
    lp.iter()
      .map(|p| get_basin_size(p, &map))
      .sorted()
      .rev()
      .take(3)
      .product::<usize>()
  );
}

fn main() {
  solution1();
  solution2();
}
