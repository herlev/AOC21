#![allow(dead_code)]
#![allow(unused_imports)]
use itertools::Itertools;
use std::collections::HashSet;
use std::fs;
use std::ops::Add;

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
struct Point {
  x: i32,
  y: i32,
}

impl Point {
  fn neighbors(&self) -> Vec<Point> {
    [
      Point { x: 1, y: 0 },
      Point { x: -1, y: 0 },
      Point { x: 0, y: 1 },
      Point { x: 0, y: -1 },
      Point { x: -1, y: -1 },
      Point { x: -1, y: 1 },
      Point { x: 1, y: 1 },
      Point { x: 1, y: -1 },
    ]
    .iter()
    .map(|&p2| *self + p2)
    .collect()
  }
}

impl Add for Point {
  type Output = Self;

  fn add(self, other: Self) -> Self::Output {
    Self {
      x: self.x + other.x,
      y: self.y + other.y,
    }
  }
}

struct Grid {
  data: [[u32; Grid::WIDTH]; Grid::HEIGHT],
}

struct GridIterator {
  index: usize,
}

impl Iterator for GridIterator {
  type Item = Point;

  fn next(&mut self) -> Option<Point> {
    if self.index >= Grid::HEIGHT * Grid::WIDTH {
      return None;
    }
    let p = Point {
      x: (self.index % Grid::WIDTH) as i32,
      y: (self.index / Grid::WIDTH) as i32,
    };
    self.index += 1;
    Some(p)
  }
}

impl Grid {
  const WIDTH: usize = 10;
  const HEIGHT: usize = 10;
  fn parse(s: &str) -> Grid {
    let d = s
      .lines()
      .map(|line| line.chars().map(|c| c.to_digit(10).unwrap()).collect())
      .collect::<Vec<Vec<u32>>>();
    assert!(d.len() == Grid::HEIGHT && d.iter().all(|row| row.len() == Grid::WIDTH));
    let mut g = Grid {
      data: [[0u32; 10]; 10],
    };
    // println!("{:?}", d);
    for x in 0..Grid::WIDTH {
      for y in 0..Grid::HEIGHT {
        *g.at(Point {
          x: x as i32,
          y: y as i32,
        }) = d[y][x];
      }
    }
    g
  }
  fn points(&self) -> GridIterator {
    GridIterator { index: 0 }
  }
  fn contains(&self, p: Point) -> bool {
    0 <= p.x && p.x < Grid::WIDTH as i32 && 0 <= p.y && p.y < Grid::HEIGHT as i32
  }
  fn at(&mut self, p: Point) -> &mut u32 {
    assert!(self.contains(p));
    &mut self.data[p.y as usize][p.x as usize]
  }
  fn print(&self) {
    let s = self
      .data
      .iter()
      .map(|row| row.map(|n| n.to_string()).join(" "))
      .join("\n");
    println!("{}\n", s);
  }
  fn flash(&mut self, p: Point) {
    p.neighbors()
      .iter()
      .filter(|&a| self.contains(*a))
      .collect::<Vec<_>>()
      .iter()
      .for_each(|&p| *self.at(*p) += 1);
  }
  fn update(&mut self) -> usize {
    for p in self.points() {
      *self.at(p) += 1;
    }
    let mut flashed_points: HashSet<Point> = HashSet::new();
    loop {
      let flash_points = self
        .points()
        .filter(|&p| *self.at(p) > 9 && !flashed_points.contains(&p))
        .collect::<Vec<Point>>();
      if flash_points.len() == 0 {
        break;
      }
      for p in flash_points {
        self.flash(p);
        flashed_points.insert(p);
      }
    }
    flashed_points.iter().for_each(|&p| *self.at(p) = 0);
    flashed_points.len()
  }
}

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

fn solution1() {
  let input = load();
  let mut g = Grid::parse(&input);
  let sum = (0..100).map(|_| g.update()).sum::<usize>();
  g.print();
  println!("total flashes: {}", sum);
}

fn solution2() {
  let input = load();
  let mut g = Grid::parse(&input);
  let mut i = 0;
  loop {
    g.update();
    i += 1;
    if g.data.iter().flatten().all(|&n| n == 0) {
      break;
    }
  }
  println!("answer: {}", i);
}

fn main() {
  solution1();
  solution2();
}
