const std = @import("std");
const input = @embedFile("input.txt");

const Direction = enum { forward, up, down };

pub fn main() !void {
    { // Part 1
        var x: usize = 0;
        var depth: usize = 0;

        var it = std.mem.tokenize(input, "\n ");
        while (it.next()) |direction| {
            var val = try std.fmt.parseInt(usize, it.next().?, 10);
            switch (std.meta.stringToEnum(Direction, direction).?) {
                .forward => {
                    x += val;
                },
                .up => {
                    depth -= val;
                },
                .down => {
                    depth += val;
                },
            }
        }
        std.debug.print("horizontal position: {}, depth: {}, product: {}\n", .{ x, depth, x * depth });
    }
    var x: isize = 0;
    var depth: isize = 0;
    var aim: isize = 0;

    var it = std.mem.tokenize(input, "\n ");
    while (it.next()) |direction| {
        var val = try std.fmt.parseInt(isize, it.next().?, 10);
        switch (std.meta.stringToEnum(Direction, direction).?) {
            .forward => {
                x += val;
                depth += aim * val;
            },
            .up => {
                aim -= val;
            },
            .down => {
                aim += val;
            },
        }
    }
    std.debug.print("horizontal position: {}, depth: {}, product: {}\n", .{ x, depth, x * depth });
}
