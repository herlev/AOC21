#![allow(dead_code)]
use std::fmt;
use std::fs;

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

#[derive(Debug, Clone, Copy)]
struct Cell {
  marked: bool,
  value: usize,
}

#[derive(Debug)]
struct Board {
  cells: Vec<Vec<Cell>>,
  num_marked_row: Vec<usize>,
  num_marked_col: Vec<usize>,
  has_won: bool,
}

impl fmt::Display for Cell {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    if self.marked {
      write!(f, "<{: >2}>", self.value)
    } else {
      write!(f, " {: >2} ", self.value)
    }
  }
}

impl fmt::Display for Board {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    for row in &self.cells {
      for cell in row {
        write!(f, "{} ", cell)?;
      }
      write!(f, "\n")?;
    }
    Ok(())
  }
}

impl Board {
  fn width(&self) -> usize {
    self.cells[0].len()
  }
  fn height(&self) -> usize {
    self.cells.len()
  }
  fn sum_of_unmarked(&self) -> usize {
    self
      .cells
      .iter()
      .flatten()
      .filter_map(|cell| if cell.marked { None } else { Some(cell.value) })
      .sum()
  }
  // Marks a cell and returns true if a bingo occurs
  fn mark(&mut self, row: usize, col: usize) -> bool {
    let marked = &mut self.cells[col][row].marked;
    if *marked {
      return false;
    }
    let a = &mut self.num_marked_col[row];
    let b = &mut self.num_marked_row[col];
    *a -= 1;
    *b -= 1;
    *marked = true;
    return (*a == 0) || (*b == 0);
  }
  fn add_num(&mut self, num: usize) -> Option<usize> {
    for (col_i, row) in self.cells.clone().iter().enumerate() {
      for (row_i, cell) in row.iter().enumerate() {
        if cell.value == num {
          if self.mark(row_i, col_i) {
            return Some(num * self.sum_of_unmarked());
          }
        }
      }
    }
    None
  }
}

impl Board {
  fn parse(s: &str) -> Self {
    let cells: Vec<Vec<Cell>> = s
      .split("\n")
      .map(|row| {
        row
          .trim()
          .split(" ")
          .filter(|s| !s.is_empty())
          .map(|num| Cell {
            value: num.parse().expect("couldn't parse number"),
            marked: false,
          })
          .collect()
      })
      .collect();
    let width = cells[0].len();
    let height = cells.len();
    let num_marked_row = vec![width; height];
    let num_marked_col = vec![height; width];
    Board {
      cells,
      num_marked_row,
      num_marked_col,
      has_won: false,
    }
  }
}

fn parse_input() -> (Vec<usize>, Vec<Board>) {
  let f = load();
  let a: Vec<&str> = f.split("\n\n").collect();
  let nums: Vec<usize> = a[0]
    .split(',')
    .map(|s| s.parse().expect(&format!("{} is not a valid number", s)))
    .collect();
  let boards: Vec<Board> = a[1..].iter().map(|s| Board::parse(s)).collect();
  (nums, boards)
}

fn main() {
  let (nums, mut boards) = parse_input();
  for n in nums {
    println!("adding number {}", n);
    for board in &mut boards {
      if board.has_won {
        continue;
      }
      if let Some(score) = board.add_num(n) {
        println!("A board has won, score is {}!", score);
        println!("{}", board);
        board.has_won = true;
        // return;
      }
      println!("{}", board);
    }
  }
}
