#![allow(dead_code)]
// use std::fmt;
use std::collections::HashMap;
use std::fs;
use std::ops::{Add, Mul, Sub};

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
struct Point {
  x: i32,
  y: i32,
}

impl Point {
  fn new(x: i32, y: i32) -> Point {
    Point { x, y }
  }
  fn signum(&self) -> Point {
    Point {
      x: self.x.signum(),
      y: self.y.signum(),
    }
  }
}

impl Sub for Point {
  type Output = Self;

  fn sub(self, other: Self) -> Self::Output {
    Self {
      x: self.x - other.x,
      y: self.y - other.y,
    }
  }
}

impl Add for Point {
  type Output = Self;

  fn add(self, other: Self) -> Self::Output {
    Self {
      x: self.x + other.x,
      y: self.y + other.y,
    }
  }
}

impl Mul<i32> for Point {
  type Output = Self;

  fn mul(self, n: i32) -> Self::Output {
    Self {
      x: self.x * n,
      y: self.y * n,
    }
  }
}

#[derive(Debug)]
struct Line {
  start: Point,
  end: Point,
}

impl Line {
  fn new(start: Point, end: Point) -> Line {
    assert!(start.x == end.x || start.y == end.y);
    Line { start, end }
  }
}

impl<'a> IntoIterator for &'a Line {
  type Item = Point;
  type IntoIter = LineIterator<'a>;
  fn into_iter(self) -> Self::IntoIter {
    LineIterator {
      line: self,
      index: 0,
    }
  }
}

struct LineIterator<'a> {
  line: &'a Line,
  index: usize,
}

impl Iterator for LineIterator<'_> {
  type Item = Point;

  fn next(&mut self) -> Option<Point> {
    let start = &self.line.start;
    let end = &self.line.end;
    let dp = *end - *start;
    assert!(dp.x == 0 || dp.y == 0 || dp.x.abs() == dp.y.abs());
    if self.index > dp.x.abs().max(dp.y.abs()) as usize {
      return None;
    }
    let p = self.line.start + dp.signum() * self.index as i32;
    self.index += 1;
    Some(p)
  }
}

fn parse_point(s: &str) -> Point {
  let mut nums = s.split(",").map(|n| n.parse::<i32>());
  Point {
    x: nums.next().unwrap().unwrap(),
    y: nums.next().unwrap().unwrap(),
  }
}

fn parse_line(points: Vec<Point>) -> Line {
  assert!(points.len() == 2);
  Line {
    start: points[0],
    end: points[1],
  }
}

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

fn main() {
  let input = load();
  let lines = input
    .split("\n")
    .filter(|line_s| !line_s.is_empty())
    .map(|line_s| {
      line_s
        .split(" -> ")
        .map(parse_point)
        .collect::<Vec<Point>>()
    })
    .map(parse_line);
  let mut map = HashMap::<Point, usize>::new();
  // let line = Line::new(Point::new(5, 1), Point::new(1, 1));
  for line in lines {
    // println!(
    //   "{:?}\n---------------------------------------------------------------",
    //   line
    // );
    for point in &line {
      // println!("{:?}", point);
      *map.entry(point).or_default() += 1;
    }
  }
  // println!("{:?}", map);
  let n = map.iter().filter(|(_, val)| *val >= &2).count();
  println!("n: {}", n)
}
