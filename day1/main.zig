const std = @import("std");
const input = @embedFile("input.txt");

pub fn main() !void {
    { // Part 1
        var it = std.mem.tokenize(input, "\n");
        var last = try std.fmt.parseInt(usize, it.next().?, 10);
        var n_greater_than_prev: usize = 0;
        while (it.next()) |line| {
            const n = try std.fmt.parseInt(usize, line, 10);
            defer last = n;
            if (n > last) {
                n_greater_than_prev += 1;
            }
            // std.debug.print("last: {}, n: {}\n", .{ last, n });
        }
        std.debug.print("{}\n", .{n_greater_than_prev});
    }
    var it = std.mem.tokenize(input, "\n");
    var w0 = try std.fmt.parseInt(usize, it.next().?, 10);
    var w1 = try std.fmt.parseInt(usize, it.next().?, 10);
    var w2 = try std.fmt.parseInt(usize, it.next().?, 10);
    var n_greater_than_prev: usize = 0;
    while (it.next()) |line| {
        const w3 = try std.fmt.parseInt(usize, line, 10);
        defer w2 = w3;
        defer w1 = w2;
        defer w0 = w1;
        if (w3 > w0) {
            n_greater_than_prev += 1;
        }
    }
    std.debug.print("{}\n", .{n_greater_than_prev});
}
