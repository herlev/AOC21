#![allow(dead_code)]
#![allow(unused_imports)]
use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::fs;

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

type GraphT<'a> = HashMap<&'a str, Vec<&'a str>>;
type VisitedT<'a> = HashSet<&'a str>;

fn traverse<'a>(pos: &'a str, graph: &'a GraphT, visited: &mut VisitedT<'a>) -> usize {
  // println!("-> {}", pos);
  if pos == "end" {
    return 1;
  }
  if pos.chars().all(char::is_lowercase) {
    visited.insert(pos);
  }
  let sum = graph
    .get(pos)
    .unwrap()
    .iter()
    .filter(|&s| !visited.contains(s))
    .collect::<Vec<_>>()
    .iter()
    .map(|s| traverse(s, graph, visited))
    .sum::<usize>();
  visited.remove(pos);
  // println!("<- {}", pos);
  sum
}

fn solution1() {
  let input = load();
  let mut graph: GraphT = HashMap::new();
  let mut visited: VisitedT = HashSet::new();
  input
    .lines()
    .filter_map(|line| line.split_once("-"))
    .for_each(|(l, r)| {
      graph.entry(l).or_default().push(r);
      graph.entry(r).or_default().push(l);
    });
  let n = traverse("start", &mut graph, &mut visited);
  println!("answer: {}", n);
}

type VisitedT2<'a> = HashMap<&'a str, usize>;

fn traverse2<'a>(
  pos: &'a str,
  graph: &'a GraphT,
  visited: &mut VisitedT2<'a>,
  path: &mut Vec<&'a str>,
) -> usize {
  path.push(pos);
  if pos == "end" {
    // println!("{}", path.join(","));
    path.pop();
    return 1;
  }
  let is_limited = pos.chars().all(char::is_lowercase) && pos != "start";
  if is_limited {
    *visited.entry(pos).or_default() += 1;
  }
  let sum = graph
    .get(pos)
    .unwrap()
    .iter()
    .filter(|&s| {
      // Probably more efficient to pass a single bool flag along when recursing
      // to specify if a small cave has been visited twice and use a HashSet
      // like in part 1 to keep track of visited caves.
      let max_visits = if *visited.values().max().unwrap_or(&0) == 2 {
        1
      } else {
        2
      };
      *visited.entry(s).or_default() < max_visits && *s != "start"
    })
    .collect::<Vec<_>>()
    .iter()
    .map(|s| traverse2(s, graph, visited, path))
    .sum::<usize>();
  if is_limited {
    let val = visited.entry(pos).or_default();
    assert!(*val != 0);
    *val -= 1;
  }
  path.pop();
  sum
}

fn solution2() {
  let input = load();
  let mut graph: GraphT = HashMap::new();
  let mut visited: VisitedT2 = HashMap::new();
  input
    .lines()
    .filter_map(|line| line.split_once("-"))
    .for_each(|(l, r)| {
      graph.entry(l).or_default().push(r);
      graph.entry(r).or_default().push(l);
    });
  let mut path = vec![];
  let n = traverse2("start", &mut graph, &mut visited, &mut path);
  println!("answer: {}", n);
}

fn main() {
  solution2();
}
