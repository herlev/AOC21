use std::fs;

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

fn parsedata() -> Vec<i32> {
  load()
    .trim()
    .split(",")
    .map(|n| n.parse().expect("Could not parse number"))
    .collect()
}

fn f(positions: Vec<i32>, target: i32) -> i32 {
  positions.iter().map(|n| (n - target).abs()).sum()
}

fn solution1() {
  // The median has the property of minimizing the sum of distances for one-dimensional data [0]
  // [0] https://en.wikipedia.org/wiki/Geometric_median
  let mut data = parsedata();
  data.sort();
  let pos = data[(data.len() / 2) - 1];
  println!("x: {}, f(x): {}", pos, f(data, pos));
}

fn fuel_cost(positions: Vec<i32>, target: i32) -> i32 {
  positions
    .iter()
    .map(|p| {
      let d = (p - target).abs();
      d * (d + 1) / 2
    })
    .sum()
}

fn solution2() {
  let data = parsedata();
  // The position can be estimated using the mean
  // f(x) = sum(((p_i-x)^2+abs(p_i-x))/2)
  // f'(x) ~= nx-sum(p)
  // minima_x = sum(p)/n = mean
  let mean = data.iter().sum::<i32>() / data.len() as i32;
  println!("n: {}, cost: {}", mean, fuel_cost(data, mean as i32));
  // If this didn't yield the correct result, search around the mean for minimum
}

fn main() {
  solution1();
  solution2();
}
