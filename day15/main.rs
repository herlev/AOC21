#![allow(dead_code)]
#![allow(unused_imports)]
use itertools::Itertools;
use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap};
use std::fs;
use std::ops::Add;

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
struct Point {
  x: i32,
  y: i32,
}

impl Point {
  fn new(x: i32, y: i32) -> Point {
    Point { x, y }
  }
  fn neighbors(&self) -> Vec<Point> {
    [
      Point { x: 1, y: 0 },
      Point { x: -1, y: 0 },
      Point { x: 0, y: 1 },
      Point { x: 0, y: -1 },
    ]
    .iter()
    .map(|&p2| *self + p2)
    .collect()
  }
}

impl Add for Point {
  type Output = Self;

  fn add(self, other: Self) -> Self::Output {
    Self {
      x: self.x + other.x,
      y: self.y + other.y,
    }
  }
}

#[derive(Debug)]
struct Mat {
  data: Vec<Vec<u32>>,
}

#[derive(PartialEq, Eq)]
struct PointWithDist {
  point: Point,
  dist: u32,
}

impl PointWithDist {
  fn new(point: Point, dist: u32) -> PointWithDist {
    PointWithDist { point, dist }
  }
}

impl Ord for PointWithDist {
  fn cmp(&self, other: &Self) -> Ordering {
    // compare other with self instead of self with other, to create min-heap
    other
      .dist
      .cmp(&self.dist)
      .then_with(|| self.point.cmp(&other.point))
  }
}

impl PartialOrd for PointWithDist {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.cmp(other))
  }
}

impl Mat {
  fn parse(s: &str) -> Mat {
    let data = s
      .lines()
      .map(|line| line.chars().map(|c| c.to_digit(10).unwrap()).collect())
      .collect::<Vec<Vec<u32>>>();
    assert!(
      data.iter().all(|row| row.len() == data[0].len()),
      "Invalid input"
    );
    Mat { data }
  }
  fn height(&self) -> usize {
    self.data.len()
  }
  fn width(&self) -> usize {
    self.data[0].len()
  }
  fn contains(&self, point: Point) -> bool {
    0 <= point.x && point.x < self.width() as i32 && 0 <= point.y && point.y < self.height() as i32
  }
  fn at(&self, point: Point) -> &u32 {
    assert!(self.contains(point));
    &self.data[point.y as usize][point.x as usize]
  }
  fn expand(&mut self) {
    let n = 5;
    let (prev_height, prev_width) = (self.height(), self.width());
    self.data = self
      .data
      .iter()
      .map(|row| {
        row
          .iter()
          .cycle()
          .take(self.width() * n)
          .map(|n| *n)
          .collect()
      })
      .cycle()
      .take(self.height() * n)
      .collect();
    for y in 0..self.height() {
      for x in 0..self.width() {
        let dist = y / prev_height + x / prev_width;
        // If n>9, wrap around to 1
        self.data[y][x] = ((self.data[y][x] + dist as u32 - 1) % 9) + 1;
      }
    }
  }
}

fn print_cost_map(cost_map: &HashMap<Point, u32>, w: usize, h: usize) {
  for y in 0..h {
    for x in 0..w {
      let p = Point::new(x as i32, y as i32);
      if let Some(cost) = cost_map.get(&p) {
        print!("{: >3} ", cost);
      } else {
        print!("  _ ");
      }
    }
    println!("");
  }
  println!("");
}

fn solution() {
  let input = load();
  let mut mat = Mat::parse(&input);
  mat.expand(); // solution2
  let start = Point::new(0, 0);
  let end = Point::new((mat.width() - 1) as i32, (mat.height() - 1) as i32);
  let mut cost_map = HashMap::new();
  let mut pq = BinaryHeap::new();
  pq.push(PointWithDist::new(start, 0));
  cost_map.insert(start, 0u32);
  'outer: for _ in 0.. {
    assert!(cost_map.len() > 0);
    let PointWithDist { point, dist } = pq.pop().unwrap();
    if dist > *cost_map.get(&point).unwrap() {
      continue;
    }
    for n in point.neighbors().iter().filter(|&p| mat.contains(*p)) {
      let risk_level = *mat.at(*n);
      let current_dist = *cost_map.get(&point).unwrap() + risk_level;
      if *n == end {
        println!("Found endpoint, cost: {}", current_dist);
        break 'outer;
      }
      let should_update = match cost_map.get(n) {
        Some(&dist) => current_dist < dist,
        None => true,
      };
      if should_update {
        cost_map.insert(*n, current_dist);
        pq.push(PointWithDist::new(*n, current_dist));
      }
    }
  }
}

fn main() {
  solution();
}
