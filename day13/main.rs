#![allow(dead_code)]
#![allow(unused_imports)]
use itertools::Itertools;
use std::collections::HashSet;
use std::fs;

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

#[derive(Eq, PartialEq, Hash, Clone, Copy, Debug)]
struct Point {
  x: i32,
  y: i32,
}

impl Point {
  fn parse(s: &str) -> Point {
    let (x_s, y_s) = s.split_once(",").unwrap();
    Point {
      x: x_s.parse().unwrap(),
      y: y_s.parse().unwrap(),
    }
  }
}

fn main() {
  let input = load();
  let (points_s, instructions_s) = input.split_once("\n\n").unwrap();
  let instructions = instructions_s
    .lines()
    .map(|line| {
      let i = line.find('=').unwrap() - 1;
      let (var, n) = line.get(i..).unwrap().split_once('=').unwrap();
      (var, n.parse::<usize>().unwrap())
    })
    .collect::<Vec<(&str, usize)>>();
  let mut points = points_s.lines().map(Point::parse).collect::<HashSet<_>>();
  for (var, fold_val) in &instructions {
    points = points
      .iter()
      .map(|p| match *var {
        "x" => {
          if p.x > *fold_val as i32 {
            Point {
              x: 2 * *fold_val as i32 - p.x,
              y: p.y,
            }
          } else {
            *p
          }
        }
        "y" => {
          if p.y > *fold_val as i32 {
            Point {
              x: p.x,
              y: 2 * *fold_val as i32 - p.y,
            }
          } else {
            *p
          }
        }
        _ => unreachable!(),
      })
      .collect();
    // break;
  }
  let (x, y): (Vec<_>, Vec<_>) = points.iter().map(|p| (p.x, p.y)).unzip();
  let max_x = *x.iter().max().unwrap();
  let max_y = *y.iter().max().unwrap();
  let mut v = vec![vec![" "; max_x as usize + 1]; max_y as usize + 1];
  for p in points {
    v[p.y as usize][p.x as usize] = "█";
  }
  let s = v.iter().map(|row| row.join("")).join("\n");
  println!("{}", s);
}
