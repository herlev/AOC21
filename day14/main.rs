#![allow(dead_code)]
#![allow(unused_imports)]
#![feature(linked_list_cursors)]
use itertools::Itertools;
use std::collections::{HashMap, LinkedList};
use std::fs;

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

fn solution1() {
  let input = load();
  let (template_s, rules_s) = input.split_once("\n\n").unwrap();
  let mut template = template_s.chars().collect::<LinkedList<_>>();

  let rules = rules_s
    .lines()
    .filter_map(|line| line.rsplit_once(" -> "))
    .map(|(f, t)| (f, t.chars().nth(0).unwrap()))
    .collect::<HashMap<&str, char>>();

  println!("{}", template.iter().join(""));
  for step in 1..=5 {
    println!("step: {}", step);
    let mut ll = template.cursor_front_mut();
    loop {
      let prev = *ll.current().unwrap();
      ll.move_next();
      let current = match ll.current() {
        Some(c) => *c,
        None => break,
      };
      let pair = [prev, current].iter().join("");
      if let Some(c) = rules.get(&pair[..]) {
        // println!("{} -> {}", pair, c);
        ll.insert_before(*c);
      }
    }
    println!("{}\n", template.iter().join(""));
  }
  let mut count_map = HashMap::new();
  for c in template {
    *count_map.entry(c).or_insert(0) += 1;
  }
  let most_common = count_map.iter().max_by_key(|&(_, v)| v).unwrap();
  let least_common = count_map.iter().min_by_key(|&(_, v)| v).unwrap();
  println!("{:?}, {:?}", most_common, least_common);
  println!("answer: {}", most_common.1 - least_common.1);
}

fn solution2() {
  let input = load();
  let (template_s, rules_s) = input.split_once("\n\n").unwrap();
  let rules = rules_s
    .lines()
    .filter_map(|line| line.rsplit_once(" -> "))
    .map(|(f, t)| {
      (
        (f.chars().nth(0).unwrap(), f.chars().nth(1).unwrap()),
        t.chars().nth(0).unwrap(),
      )
    })
    .collect::<HashMap<(char, char), char>>();

  let mut map: HashMap<(char, char), usize> = HashMap::new();
  {
    let mut it = template_s.chars();
    let mut prev = it.next().unwrap();
    for c in it {
      *map.entry((prev, c)).or_default() += 1;
      prev = c;
    }
  }
  // println!("rules: {:?}", rules);

  for step in 1..=40 {
    // println!("step: {}", step);
    for (key, val) in map.clone() {
      if val == 0 {
        continue;
      }
      if let Some(&c) = rules.get(&key) {
        *map.entry((key.0, c)).or_default() += val;
        *map.entry((c, key.1)).or_default() += val;
        *map.entry(key).or_default() -= val;
      }
      // println!("key: {:?}, val: {:?}", key, val);
    }
    // println!("map: {:?}", map);
  }
  // println!("map: {:?}", map);

  let mut count_map: HashMap<char, usize> = HashMap::new();
  map.iter().for_each(|(k, v)| {
    *count_map.entry(k.0).or_default() += v;
    *count_map.entry(k.1).or_default() += v;
  });
  let first_char = template_s.chars().next().unwrap();
  let last_char = template_s.chars().last().unwrap();
  *count_map.entry(first_char).or_default() += 1;
  *count_map.entry(last_char).or_default() += 1;
  count_map.iter_mut().for_each(|(_, v)| *v /= 2);
  println!("count_map: {:?}", count_map);

  let most_common = count_map.iter().max_by_key(|&(_, v)| v).unwrap();
  let least_common = count_map.iter().min_by_key(|&(_, v)| v).unwrap();
  println!("{:?}, {:?}", most_common, least_common);
  println!("answer: {}", most_common.1 - least_common.1);
}

fn main() {
  // solution1();
  solution2();
}
