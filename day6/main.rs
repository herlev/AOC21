#![allow(dead_code)]
use std::fs;

fn tick(data: Vec<u32>) -> Vec<u32> {
  let mut num_new = 0;
  let mut new_data: Vec<u32> = data
    .iter()
    .map(|n| {
      if *n == 0 {
        num_new += 1;
        6
      } else {
        *n - 1
      }
    })
    .collect();
  let mut new = vec![8u32; num_new];
  new_data.append(&mut new);
  new_data
}

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

fn parsedata() -> Vec<u32> {
  load()
    .trim()
    .split(",")
    .map(|n| n.parse().expect("Could not parse number"))
    .collect()
}

fn solution1() {
  // Inefficient solution
  let mut data = parsedata();
  for _ in 0..80 {
    data = tick(data);
  }
  println!("n: {}", data.len());
}

#[derive(Debug)]
struct FIFO {
  data: [u64; 9],
  start_i: usize,
}

impl FIFO {
  fn new() -> FIFO {
    FIFO {
      data: [0; 9],
      start_i: 0,
    }
  }
  fn push(&mut self, v: u64) {
    self.data[self.start_i] = v;
    self.start_i = (self.start_i + 1) % self.data.len();
  }
  fn at(&mut self, i: usize) -> &mut u64 {
    &mut self.data[(self.start_i + i) % self.data.len()]
  }
}

fn solution2() {
  let mut fifo = FIFO::new();
  let data = parsedata();
  data.iter().for_each(|n| {
    assert!(*n < fifo.data.len() as u32);
    *fifo.at(*n as usize) += 1;
  });
  for _ in 0..256 {
    let n = *fifo.at(0);
    fifo.push(n);
    *fifo.at(6) += n;
  }
  println!("n: {}", fifo.data.iter().sum::<u64>());
}

fn main() {
  // println!("solution1");
  // solution1();
  println!("solution2");
  solution2();
}
