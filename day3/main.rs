#![allow(dead_code)]
use std::fs;

fn load() -> String {
    let file = "input.txt";
    fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

fn parse_input() -> Vec<Vec<usize>> {
    load()
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| match c {
                    '1' => 1,
                    '0' => 0,
                    _ => panic!("invalid input"),
                })
                .rev()
                .collect()
        })
        .collect()
}

fn get_most_common(l: &Vec<Vec<usize>>) -> Vec<isize> {
    let width = l[0].len();
    let mut most_common: Vec<isize> = vec![0; width];
    for bits in l {
        for (i, bit) in bits.iter().enumerate() {
            most_common[i] += match bit {
                1 => 1,
                0 => -1,
                _ => panic!("pls no"),
            };
        }
    }
    most_common.iter().map(|n| n.signum()).collect()
}

fn part1() {
    let most_common = get_most_common(&parse_input());
    let mut gamma = 0;
    let mut epsilon = 0;
    for (i, mc) in most_common.iter().enumerate() {
        gamma |= ((mc > &0) as u32) << i;
        epsilon |= ((mc <= &0) as u32) << i;
    }
    println!("{:?}", most_common);
    println!("{:b}", gamma);
    println!("{:b}", epsilon);
    println!("{}", gamma * epsilon);
}

enum RatingType {
    OxygenGenerator,
    Co2Scrubber,
}

fn to_usize(bin_num: Vec<usize>) -> usize {
    let mut res = 0;
    for (i, bit) in bin_num.iter().enumerate() {
        res |= bit << i;
    }
    res
}

fn get_generator_ratings(input: &Vec<Vec<usize>>, r: RatingType) -> Vec<usize> {
    let mut l = input.clone();
    let width = l[0].len();
    let is_ogr = match r {
        RatingType::OxygenGenerator => true,
        RatingType::Co2Scrubber => false,
    };
    for i in (0..width).rev() {
        let most_common = get_most_common(&l);
        l.retain(|bits| {
            bits[i]
                == match most_common[i] {
                    1 => is_ogr as usize,
                    0 => is_ogr as usize,
                    -1 => !is_ogr as usize,
                    _ => unreachable!(),
                }
        });
        if l.len() <= 1 {
            break;
        }
    }
    l[0].to_owned()
}

fn part2() {
    let input = parse_input();
    let ogr = get_generator_ratings(&input, RatingType::OxygenGenerator);
    let csr = get_generator_ratings(&input, RatingType::Co2Scrubber);
    println!("{:?}", ogr);
    println!("{:?}", csr);
    println!("{}", to_usize(ogr) * to_usize(csr));
}

fn main() {
    // part1();
    part2();
}
