const std = @import("std");
const input = @embedFile("testinput.txt");
const List = std.ArrayList;
const test_allocator = std.testing.allocator;
const print = std.debug.print;

// fn filter(comptime T: type, l: List(T), allocator: *std.mem.Allocator, f: fn (T) bool) !List(T) {
//     var list = List(T).init(allocator);
//     for (l.items) |item| {
//         if (f(item)) {
//             try list.append(item);
//         }
//     }
//     return list;
// }
fn sign(n: isize) isize {
    if (n > 0) {
        return 1;
    } else if (n < 0) {
        return -1;
    }
    return 0;
}

fn getBit(n: usize, bit: usize) u1 {
    return (n >> bit) & 1;
}

const parseInput_t = struct {width: usize, list: List(usize)};
fn parseInput(allocator: *std.mem.Allocator) !List(List(u1)) { // return tuple if possible
    var it = std.mem.tokenize(input, "\n");
    const width: usize = it.next().?.len;
    it.reset();
    var l = List(List(u1)).init(allocator);
    while (it.next()) |bits| {
        var sub_list = List(u1).init(allocator);
        try sub_list.appendNTimes(0, width);
        for (bits) |bit, i| {
            if (i >= width) unreachable;
            sub_list.items[width-1-i] = switch(bit) {
                '0' => 0,
                '1' => 1,
                else => unreachable,
            };
        }
        try l.append(sub_list);
        // const n = try std.fmt.parseInt(usize, bits, 2);
        // try l.append(n);
    }
    return l;
}

fn getMostCommon(width: usize, l: List(List(u1)), allocator: *std.mem.Allocator) !List(isize) {
    var list = List(isize).init(allocator);
    try list.appendNTimes(0, width);
    for (l.items) |bits| {
        // var i = 0;
        // while (i<width) : (i+=1) {
        //     list.items[i] += switch (getBit(bits, i)) {
        //         0 => -1,
        //         1 => 1,
        //     };
        // }
        for (bits.items) |bit, i| {
            list.items[i] += switch(bit) {
                0 => @as(isize, -1),
                1 => 1,
            };
        }
    }
    for (list.items) |*n| {
        n.* = sign(n.*);
    }
    return list;
}

fn getOGR(l: List(usize), most_common: List(isize), width: usize, allocator: *std.mem.Allocator) !usize {
    var filtered_list = List(usize).init(allocator);
    for (l.items) |n| {
        try filtered_list.append(n);
    }
    var i: usize = 0;
    while (i < width) : (i += 1) {
        var nfl = List(usize).init(allocator);
        for (filtered_list.items) |e| {
            // if getbit e,i == 1
            // nfl.append e
        }
        filtered_list.deinit();
        filtered_list = nfl;
        if (filtered_list.items.len <= 1) {
            break;
        }
    }
    return 0;
}
// for (0..width) |i|
//     mc = getMostCommon
//     :

pub fn main() !void {
    const allocator = std.heap.page_allocator;
    // { // Part 1
    // var it = std.mem.tokenize(input, "\n ");
    // const width = it.next().?.len;
    // it.reset();

    // var list = List(isize).init(allocator);
    // try list.appendNTimes(0, width);
    // defer list.deinit();

    // while (it.next()) |bits| {
    //     // print("{s}\n", .{bits});
    //     for (bits) |bit, i| {
    //         list.items[width - 1 - i] += switch (bit) {
    //             '0' => @as(isize, -1),
    //             '1' => 1,
    //             else => unreachable,
    //         };
    //     }
    // }
    // var gamma_rate: usize = 0;
    // var epsilon_rate: usize = 0;
    // for (list.items) |n, I| {
    //     var i = @intCast(u6, I);
    //     gamma_rate |= @as(usize, @boolToInt(n > 0)) << i;
    //     epsilon_rate |= @as(usize, @boolToInt(n <= 0)) << i;
    // }
    // print("g {b}, e {b}\n", .{ gamma_rate, epsilon_rate });
    // print("{}\n", .{gamma_rate * epsilon_rate});
    var l = try parseInput(allocator); // Destructure the struct here instead? (if possible)
    var list = try getMostCommon(l.items[0].items.len, l, allocator);
    // _ = try getOGR(l, list, width, allocator);
    var ogr: usize = 0;
    var csr: usize = 0;
    // Create arraylist with bits
    // filter based on most/least common
    print("{}\n", .{list});
}
