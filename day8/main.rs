#![allow(dead_code)]
use itertools::Itertools;
use std::fs;

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

fn solution1() {
  let input = load();
  let a = input
    .split("\n")
    .map(|line| line.split(" | ").collect::<Vec<&str>>())
    .map(|line| line[1].split(" "))
    .flatten()
    .filter(|s| s.len() == 2 || s.len() == 3 || s.len() == 4 || s.len() == 7)
    .count();
  println!("{:?}", a);
}

fn intersection(a: &str, b: &str) -> usize {
  a.chars()
    .map(|c| match b.find(c) {
      Some(_) => 1,
      None => 0,
    })
    .sum()
}

type LineT<'a> = (Vec<String>, Vec<String>);
fn get_digits(s: Vec<&str>) -> [&str; 10] {
  let mut vals: [&str; 10] = [""; 10];
  let g1478 = s.iter().filter(|s| match s.len() {
    2 | 3 | 4 | 7 => true,
    _ => false,
  });
  let g069 = s.iter().filter(|s| s.len() == 6);
  let g235 = s.iter().filter(|s| s.len() == 5);
  // populate 1, 4, 7, 8
  g1478.for_each(|s| match s.len() {
    2 => vals[1] = s,
    3 => vals[7] = s,
    4 => vals[4] = s,
    7 => vals[8] = s,
    _ => unreachable!(),
  });
  // populate 6, 9, 0
  g069.for_each(|s| match intersection(s, vals[1]) {
    1 => vals[6] = s,
    2 => match intersection(s, vals[4]) {
      3 => vals[0] = s,
      4 => vals[9] = s,
      _ => unreachable!(),
    },
    _ => unreachable!(),
  });
  // populate 3, 5, 2
  g235.for_each(|s| {
    if intersection(s, vals[1]) == 2 {
      vals[3] = s;
    } else {
      match intersection(s, vals[6]) {
        4 => vals[2] = s,
        5 => vals[5] = s,
        _ => unreachable!(),
      }
    }
  });
  assert!(vals.iter().all(|s| !s.is_empty()));
  // Maybe return a map here instead, so that position() isn't needed get_output
  vals
}

fn parse_line(line: &str) -> LineT {
  line
    .split(" | ")
    .map(|half_line| {
      half_line
        .split(" ")
        .map(|digits| digits.chars().sorted().collect::<String>())
        .collect::<Vec<_>>()
    })
    .collect_tuple()
    .unwrap()
}

fn get_output(line: LineT) -> usize {
  let thing = get_digits(line.0.iter().map(|s| s.as_ref()).collect());
  let mut result = 0;
  line.1.iter().rev().enumerate().for_each(|(i, s)| {
    result += 10usize.pow(i as u32) * thing.iter().position(|&r| r == s).unwrap()
  });
  result
}

fn solution2() {
  let input = load();
  let answer: usize = input.lines().map(parse_line).map(get_output).sum();
  println!("answer: {:?}", answer);
}

fn main() {
  solution1();
  assert!(intersection("aclj", "abc") == 2);
  assert!(intersection("lkjoij", "abc") == 0);
  solution2();
}
