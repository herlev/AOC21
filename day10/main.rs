#![allow(dead_code)]
#![allow(unused_imports)]
use bimap::BiMap;
use itertools::Itertools;
use std::collections::HashMap;
use std::fs;

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

fn get_first_illegal_character(line: &str) -> Option<char> {
  let map = BiMap::from_iter(vec![('(', ')'), ('{', '}'), ('[', ']'), ('<', '>')]);
  let mut stack: Vec<char> = vec![];
  for c in line.chars() {
    if map.contains_left(&c) {
      stack.push(c)
    } else if map.contains_right(&c) {
      match stack.pop() {
        Some(start_c) => {
          if *map.get_by_right(&c).unwrap() != start_c {
            return Some(c);
          }
        }
        None => unreachable!(),
      }
    } else {
      unreachable!();
    }
  }
  None
}

fn get_completion_string(line: &str) -> Option<String> {
  let map = BiMap::from_iter(vec![('(', ')'), ('{', '}'), ('[', ']'), ('<', '>')]);
  let mut stack: Vec<char> = vec![];
  for c in line.chars() {
    if map.contains_left(&c) {
      stack.push(c)
    } else if map.contains_right(&c) {
      match stack.pop() {
        Some(end_c) => {
          if *map.get_by_right(&c).unwrap() != end_c {
            return None;
          }
        }
        None => unreachable!(),
      }
    } else {
      unreachable!();
    }
  }
  Some(
    stack
      .iter()
      .rev()
      .map(|c| map.get_by_left(&c).unwrap())
      .collect::<String>(),
  )
}

fn solution1() {
  let score_map = HashMap::from([(')', 3), (']', 57), ('}', 1197), ('>', 25137)]);
  let input = load();
  let syntax_error_score = input
    .lines()
    .filter_map(get_first_illegal_character)
    .map(|c| score_map.get(&c).unwrap())
    .sum::<usize>();
  println!("answer: {}", syntax_error_score);
}

fn solution2() {
  let score_map = HashMap::from([(')', 1), (']', 2), ('}', 3), ('>', 4)]);
  let input = load();
  let completion_scores = input
    .lines()
    .filter_map(get_completion_string)
    .map(|cs| {
      cs.chars()
        .fold(0, |acc, c| acc * 5 + score_map.get(&c).unwrap())
    })
    .sorted()
    .collect::<Vec<usize>>();
  println!(
    "answer: {:?}",
    completion_scores[completion_scores.len() / 2]
  );
}

fn main() {
  solution1();
  solution2();
}
