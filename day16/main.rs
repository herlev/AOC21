#![allow(dead_code)]
#![allow(unused_imports)]
use itertools::Itertools;
use std::fs;

fn load() -> String {
  let file = "input.txt";
  fs::read_to_string(&file).unwrap_or_else(|_| panic!("Error loading file"))
}

type BitVec = Vec<bool>;

fn to_bitvec(c: char) -> BitVec {
  let n = c.to_digit(16).unwrap();
  (0..4).rev().map(|i| n & (1 << i) > 0).collect()
}

fn parse_bits(s: &str) -> impl Iterator<Item = bool> + '_ {
  s.chars().flat_map(to_bitvec)
}

fn to_u64(bitvec: &BitVec) -> u64 {
  assert!(bitvec.len() <= 64);
  let mut n = 0;
  for (i, b) in bitvec.iter().rev().enumerate() {
    n |= (*b as u64) << i;
  }
  n
}

fn to_u8_v(bits: &BitVec) -> Vec<u8> {
  bits.iter().map(|b| if *b { 1 } else { 0 }).collect()
}

fn take_n_bits_to_u64(bits: &mut dyn Iterator<Item = bool>, n: u8) -> u64 {
  assert!(n <= 64);
  to_u64(&bits.take(n.into()).collect::<Vec<_>>())
}

#[derive(Debug)]
enum PacketData {
  Literal(u64),
  Operator(Vec<Packet>),
}

#[derive(Debug)]
struct Packet {
  version: u8,
  type_id: u8,
  data: PacketData,
}

impl Packet {
  fn version_sum(&self) -> u32 {
    self.version as u32
      + match &self.data {
        PacketData::Literal(_) => 0,
        PacketData::Operator(packets) => packets.iter().map(|packet| packet.version_sum()).sum(),
      }
  }
  fn value(&self) -> u64 {
    match &self.data {
      PacketData::Literal(l) => *l,
      PacketData::Operator(packets) => {
        let values = packets.iter().map(|packet| packet.value());
        match self.type_id {
          0 => values.sum(),
          1 => values.product(),
          2 => values.min().unwrap(),
          3 => values.max().unwrap(),
          5 | 6 | 7 => {
            assert!(packets.len() == 2);
            match self.type_id {
              5 => (packets[0].value() > packets[1].value()) as u64,
              6 => (packets[0].value() < packets[1].value()) as u64,
              7 => (packets[0].value() == packets[1].value()) as u64,
              _ => unreachable!(),
            }
          }
          _ => unreachable!(),
        }
      }
    }
  }
}

fn parse_literal(bits: &mut dyn Iterator<Item = bool>) -> u64 {
  let mut bv: Vec<bool> = Vec::new();
  loop {
    let is_last = !bits.next().unwrap();
    let mut b: Vec<_> = bits.take(4).collect();
    assert!(b.len() == 4);
    bv.append(&mut b);
    if is_last {
      break;
    }
  }
  to_u64(&bv)
}

fn parse_operator(mut bits: &mut dyn Iterator<Item = bool>) -> Vec<Packet> {
  let length_type_id = bits.next().unwrap() as u8;
  let mut packets: Vec<Packet> = Vec::new();
  if length_type_id == 0 {
    let total_bit_length = take_n_bits_to_u64(&mut bits, 15);
    let mut sub_packet_bits = bits.take(total_bit_length as usize).peekable();
    while let Some(_) = sub_packet_bits.peek() {
      packets.push(parse_packet(&mut sub_packet_bits));
    }
  } else {
    let num_packets = take_n_bits_to_u64(&mut bits, 11);
    for _ in 0..num_packets {
      packets.push(parse_packet(&mut bits));
    }
  }
  packets
}

fn parse_packet(mut bits: &mut dyn Iterator<Item = bool>) -> Packet {
  let version = take_n_bits_to_u64(&mut bits, 3) as u8;
  let type_id = take_n_bits_to_u64(&mut bits, 3) as u8;
  Packet {
    version,
    type_id,
    data: match type_id {
      4 => PacketData::Literal(parse_literal(&mut bits)),
      _ => PacketData::Operator(parse_operator(&mut bits)),
    },
  }
}

fn get_version_sum(s: &str) -> u32 {
  let mut bits = parse_bits(s);
  let packet = parse_packet(&mut bits);
  packet.version_sum()
}

fn test_version_sum() {
  assert_eq!(get_version_sum("8A004A801A8002F478"), 16);
  assert_eq!(get_version_sum("620080001611562C8802118E34"), 12);
  assert_eq!(get_version_sum("C0015000016115A2E0802F182340"), 23);
  assert_eq!(get_version_sum("A0016C880162017C3686B18A3D4780"), 31);
}

fn solution1() {
  let input = load();
  println!("answer: {}", get_version_sum(&input));
}

fn solution2() {
  let input = load();
  println!("answer: {}", parse_packet(&mut parse_bits(&input)).value());
}

fn main() {
  test_version_sum();
  solution1();
  solution2();
}
